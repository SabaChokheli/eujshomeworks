
var myGamePiece;

function startGame() {
    myGameArea.start();
    myGamePiece = new component(40, 40, "green", 140, 140, 40);
    myGamePiece_2 = new component(40, 40, "red", 140, 140, 40);
}

var myGameArea = {
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.width = 500;
        this.canvas.height = 300;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.interval = setInterval(updateGameArea, 20);
        window.addEventListener('keydown', function (e) {
            myGameArea.key = e.keyCode;
        })
        window.addEventListener('keyup', function (e) {
            myGameArea.key = false;
        })
    }, 
    clear : function(){
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

function component(width, height, color, x, y, z) {
    this.gamearea = myGameArea;
    this.width = width;
    this.height = height;
    this.speedX = 0;
    this.speedY = 0;    
    this.speedZ = 0;
    this.x = x;
    this.y = y;
    this.z = z;   
    this.update = function() {
        ctx = myGameArea.context;
        ctx.fillStyle = color;
        ctx.fillRect(this.x, this.y, this.z, this.width, this.height);
    }
    this.newPos = function() {
        this.x += this.speedX;
        this.y += this.speedY;  
        this.z += this.speedZ;      
    }
}

function updateGameArea() {
    myGameArea.clear();
    myGamePiece.speedX = 0;
    myGamePiece.speedY = 0;    
    myGamePiece.speedZ = 0;
    if (myGameArea.key && myGameArea.key == 37) {myGamePiece.speedZ = -1.8; }
    if (myGameArea.key && myGameArea.key == 39) {myGamePiece.speedZ = 1.8; }
    if (myGameArea.key && myGameArea.key == 38) {myGamePiece.speedY = -1.8; }
    if (myGameArea.key && myGameArea.key == 40) {myGamePiece.speedY = 1.8; }
    myGamePiece.newPos();    
    myGamePiece.update();
}