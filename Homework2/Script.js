// Homework 1

let x1 = 67.259;

alert(Math.floor(x1));

// Homework 2

let x2 = 33.341;

alert(Math.ceil(x2));

// Homework 3

let x3 = 13.189;

alert(Math.round(x3));

// Homework 4

alert(Math.floor(Math.random() * 11));

// Homework 5

alert(Math.floor(Math.random() * 5));

// Homework 6

alert(Math.floor(Math.random() * 50) + 5);

//Homework 7

function randomnum(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

document.write(randomnum(4, 14));
document.write("<br><br>");

//Homework 11

const days = ["ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა"];

const random = Math.floor(Math.random() * days.length);
document.write(days[random]);
document.write("<br><br>");

//Homework 12

const potos = ["/Pics/poto1.jpg", "/poto2.png", "/Pics/poto3.jpg", "/Pics/poto4.jpg", "/Pics/poto5.jpg", "/Pics/poto6.jpg", "/Pics/poto7.jpg", "/Pics/poto8.jpg", "/Pics/poto9.jpg", "/Pics/poto10.jpg"];

const random = Math.floor(Math.random() * potos.length);
document.write(potos[random]);
document.write("<br><br>");