//დავალება 1
function task1(){
    document.write("<h5>დავალება 1</h5>")
    document.write ("<p class='dec1'>Saba Chokheli</p><hr>")
    }
    task1()

//დავალება 2
    function task2(num1, num2){
        document.write("<h5>დავალება 2</h5> <hr>")
        return num1+num2;
    }
    task2()

//დავალება 3
    function task3(){
        document.write("<h5>დავალება 3</h5>")
        document.write("<p class='dec2'>Saba Chokheli</p><hr>")
    }
    task3()

//დავალება 4
    function task4(width=150){
        document.write("<h5>დავალება 4</h5>")
        tb = "<table class='tb-style' style='width:"+width+"px; height: 150px;'>"
        
            for (i=0; i<2; i++){
                tb += "<tr>"
                for(j=0; j<2; j++){
                    tb += "<td>"
                    tb += "</td>"
                }
                tb+= "</tr>"
            }
        tb += "</table>"
        document.write(tb)
        document.write("<hr>")
    }
    task4(200)

//დავალება 5
    function task5(height=150){
        document.write("<h5>დავალება 5</h5>")
        tb = "<table class='tb-style' style='width:150px; height: "+height+"px;'>"
        
            for (i=0; i<2; i++){
                tb += "<tr>"
                for(j=0; j<2; j++){
                    tb += "<td>"
                    tb += "</td>"
                }
                tb+= "</tr>"
            }
        tb += "</table>"
        document.write(tb)
        document.write("<hr>")
    }
    task5(100)

//დავალება 6
    function task6(width=150, height=150){
        document.write("<h5>დავალება 6</h5>")
        tb = "<table class='tb-style' style='width: "+width+"px; height: "+height+"px;'>"
        
            for (i=0; i<2; i++){
                tb += "<tr>"
                for(j=0; j<2; j++){
                    tb += "<td>"
                    tb += "</td>"
                }
                tb+= "</tr>"
            }
        tb += "</table>"
        document.write(tb)
        document.write("<hr>")
    }
    task6(100, 100)

//დავალება 7
    function task7(bgcolor="white"){
        document.write("<h5>დავალება 7</h5>")
        tb = "<table class='tb-style' style='width: 150px; height: 150px; background-color:"+bgcolor+"'>"
        
            for (i=0; i<2; i++){
                tb += "<tr>"
                for(j=0; j<2; j++){
                    tb += "<td>"
                    tb += "</td>"
                }
                tb+= "</tr>"
            }
        tb += "</table>"
        document.write(tb)
        document.write("<hr>")
    }
    task7('lime')

//დავალება 9
    function task9(width=150, height=150, bgcolor="white"){
        document.write("<h5>დავალება 9</h5>")
        tb = "<table class='tb-style' style='width: "+width+"px; height: "+height+"px; background-color:"+bgcolor+"'>"
        
            for (i=0; i<2; i++){
                tb += "<tr>"
                for(j=0; j<2; j++){
                    tb += "<td>"
                    tb += "</td>"
                }
                tb+= "</tr>"
            }
        tb += "</table>"
        document.write(tb)
        document.write("<hr>")
    }
    task9(190, 100, 'violet')

//დავალება10
    function task10(width=150, height=150, bgcolor="white"){
        document.write("<h5>დავალება 10</h5>")
        tb = "<table class='tb-style' style='width: "+width+"px; height: "+height+"px; background-color:"+bgcolor+"'>"
        
            for (i=0; i<2; i++){
                tb += "<tr>"
                for(j=0; j<2; j++){
                    tb += "<td>"
                    tb += "</td>"
                }
                tb+= "</tr>"
            }
        tb += "</table>"
        document.write(tb)
        document.write("<hr>")
    }
    task10(130, 140, 'red')

//დავალება11
    function task11(){
        document.write("<h5>დავალება 11</h5>")
        for(i=0; i<10; i++)
        document.write("<p>"+ i +" </p>")
        document.write("<hr>")
    }
    task11()

//დავალება 12
    function task12(j){
        document.write("<h5>დავალება 12</h5>")
        for(i=1; i< j; i++)
        document.write("<p>"+ i +" </p>")
        document.write("<hr>")
    }
    task12(5)



//დავალება 15
    function task15(rows=2, collums=2){
        document.write("<h5>დავალება 15</h5>")
        tb = "<table class='tb-style' style='width: 150px; height: 150px;'>"
        
            for (i=0; i<rows; i++){
                tb += "<tr>"
                for(j=0; j<collums; j++){
                    tb += "<td>"
                    tb += "</td>"
                }
                tb+= "</tr>"
            }
        tb += "</table>"
        document.write(tb)
        document.write("<hr>")
    }
    task15(4, 4)

//დავალება 17
    function task17(rows=2, collums=3, width=150, height=150, bgcolor="white"){
    document.write("<h5>დავალება 17</h5>")
    tb = "<table class='tb-style' style='width:"+width+"px; height:"+height+"px; background-color:"+bgcolor+"'>"
        
        for(i=0; i<rows; i++){
            tb += "<tr>"
                for(j=0; j<collums; j++){
                tb += "<td>"
                tb += "</td>"
                }
            tb += "</tr>"
        }

    tb += "</table>"
    document.write(tb)
    }
    task17(5, 7, 400, 200, 'skyblue')
