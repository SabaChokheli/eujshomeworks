//Homework 1
function work1(){
    document.write("დავალება 1","<br>")
    let str = "Hello world, welcome to the universe";
    document.write(str.length)
    document.write("<br>")
}
work1()

//Homework 2
function work2(){
    document.write("დავალება 2","<br>")
    const str = "Hello world, welcome to the universe";
    document.write(str.split('e').length)
    document.write("<br>")
}
work2()

//Homework 3
function work3(){
    document.write("დავალება 3","<br>")
    const str = "Hello world, welcome to the universe";
    document.write(str.indexOf("world"))
    document.write("<br>")
}
work3()

//Homework 4
function work4(){
    document.write("დავალება 4","<br>")
    const str = "Hello world, welcome to the universe";
    document.write(str.charCodeAt(2))
    document.write("<br>")
}
work4()

//Homework 6??
function work6(){
    document.write("დავალება 6","<br>")
    const str = "Hello world, welcome to the universe";
    document.write(str.random(5))
    document.write("<br>")
}
work6()